The status endpoint returns information about the connection towards DGC. The value returned can be one of the following:
* __connected__: a connection with DGC has been established successfully
* __authentication_issues__: a connection with DGC has not been established because of wrong credentials
* __connection_issues__: a connection with DGC has not been established because of connection issues. In most cases this is a result of a wrong basepath URL.