A mapping file is a document containing all transformation rules needed to translate source-system specific data to DGC format. This file needs to be created before an application can be registered with the Connect Hub API.

One mapping file can be reused by several applications and is identified by it's name, which is unique.