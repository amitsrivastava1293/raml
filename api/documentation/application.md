Applications are third party applications that use the Connect Hub API to upsert data in DGC. Each application needs to refer to a mapping, which is needed to transform the data received to DGC format. 

To register an application, one needs to pass the following information:
 - name
 - version
 - sourceSystem
 - mappingName
 - description (Optional)
 - callbackEndpoint (Optional)
 
Once an application is successfully registered, the API returns back a unique identifier. This needs to be saved for later reuse. 