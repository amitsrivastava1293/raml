#%RAML 1.0 DataType 

type: object
properties:
    BillToAccountBoltID:
        type: string
        minLength: 1
    CreatedDate:
        type: string | nil
    AccountName:
        type: string | nil
        required: false
    BoltAccountNumber:
        type: string | nil
        minLength: 1
    AccountAddress:
        type: array
        minItems: 3
		items: !include accountAddress.raml
    